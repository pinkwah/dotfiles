# -*- mode: fish; fish-indent-offset: 2 -*-
set -g fish_color_command green

function fish_remove_path
    if set -l index (contains -i $argv[1] $PATH)
        set --erase PATH[$index]
    end
end

# ----------------------------------------------------------
# Environment variables
# ----------------------------------------------------------
set -gx PYTHONDONTWRITEBYTECODE "Use ~/.cache next time, Guido"

# ----------------------------------------------------------
# Simple aliases and tools
# ----------------------------------------------------------
alias d='ls -D'
alias ad='ls -aD'
alias l='ls -lh'
alias ll='ls -lha'

if type rbenv >/dev/null 2>&1
    status --is-interactive; and source (rbenv init -|psub)
end

if type nodenv >/dev/null 2>&1
    status --is-interactive; and source (nodenv init -|psub)
end

if type direnv >/dev/null 2>&1
    direnv hook fish | source
end
