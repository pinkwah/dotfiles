{ pkgs, inputs, config, lib, ... }:

let
  python = pkgs.python311;
  pkgs-stable = import inputs.nixpkgs-stable { system = pkgs.stdenv.system; };

  python-pkgs = with pkgs-stable.python311Packages; [
    # PyQt 5
    pyqt5
    pyqt5_sip

    # PySide 6
    pyside6
    shiboken6
  ];
in {
  env = {
    LC_NUMERIC = "C";
    PYTHONPATH = lib.concatMapStringsSep ":" (p: "${p}/${python.sitePackages}") python-pkgs;
    QT_PLUGIN_PATH = "${config.env.DEVENV_PROFILE}/${pkgs.qt5.qtbase.qtPluginPrefix}";

    CMAKE_GENERATOR = "Ninja";
    CMAKE_EXPORT_COMPILE_COMMANDS = "1";

    SETUPTOOLS_SCM_PRETEND_VERSION = "0.0.0";

    MOCK_AZURE_INSTANCE = "1";
  };
 
  languages.python = {
    enable = true;
    package = python;
    libraries = [pkgs.stdenv.cc.cc.lib];
    manylinux.enable = false;
    venv.enable = true;
  };

  packages = with pkgs-stable; [
    # C++ requirements
    cmake
    ninja
    backward-cpp
    fmt.dev
    catch2

    # Misc
    cmake-format
    python.pkgs.pybind11

    # Qt5
    qt5.qtbase
    qt5.qtsvg
    qt5.qtwayland
    qt5.qtx11extras

    # Dotnet
    dotnet-sdk_8
  ];
}
